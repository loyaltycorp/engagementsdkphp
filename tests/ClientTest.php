<?php

use EngagementSdk\Engagement;
use EngagementSdk\Event;

require __DIR__ . '/../vendor/autoload.php';

/*
 * https://api-staging.engagement.net.au/api/documentation
 */

class ClientTest extends TestCase {

    /** @var Engagement $client */
    private $client;

    // https://engagement-api-staging.loyaltycorp.com.au/v1/
    private $endpoint;
    private $vendorKey;
    private $vendorName;

    public function setUp() {
        parent::setUp();
        $this->endpoint = getenv('API_URL');
        $this->vendorKey = getenv('API_KEY');
        $this->vendorName = getenv('API_VENDOR');
        $this->client = new Engagement($this->endpoint, $this->vendorKey);
    }

    public function tearDown() {
        $this->client = null;
        parent::tearDown();
    }

    /**
     * @group VendorGroup
     * @var \EngagementSdk\Response $response
     */
    public function testCreateProject() {
        $name = 'Test Project - ' . microtime(true);

        $response = $this->client
            ->project()
            ->create([
                'name' => $name,
            ]);


        if ($response->isError()) {
            $response->errorToException();
        }

        $this->assertSame($response->data->name, $name);
        return $response->data;
    }


	/**
	 * list projects (should be one)
	 * @group VendorGroup
	 * @var \EngagementSdk\Response $response
	 */
	public function testListProjects() {
        $response = $this->client
            ->project()
            ->get(true);

        if ($response->isError()) {
            $response->errorToException();
        }

        $this->assertTrue(is_array($response->data));
        $this->assertCount(1, $response->data);
    }

    /**
     * @depends testCreateProject
     * @param $project
     */
    public function testFindProject($project) {
        $response = $this->client
            ->project($project->hash_id)
            ->find();

        $this->assertSame($project->hash_id, $response->data->hash_id);
    }

    /**
     * @depends testCreateProject
     * @param $project
     */
    public function testCreateAction($project) {
        $response = $this->client
	        ->project($project->hash_id)
            ->action()
            ->create([
                'name' => 'test_action',
                'description' => 'this is a test action for testing the engagement client',
                'active' => true
            ]);

        $this->assertNotEmpty($response->data);
        $this->assertSame('test_action', $response->data->name);

        return $response->data;
    }


    /**
     * @depends testCreateProject
     * @param $project
     * @return
     */
    public function testIdentifyMemberCreate($project) {
        $token = $project->token;
        $e = new Engagement($this->endpoint, $token);
        $data = [
            'customer_id' => time(),
            'name' => 'Test User',
            'email' => 'test@example.org'
        ];

        $response = $e
            ->member()
            ->identify($data);
            //->getWithJsonPayload($data);

        $this->assertNotEmpty($response->data);
        $this->assertSame($data['customer_id'], $response->data->customer_id);
        $this->assertSame($data['name'], $response->data->name);
        $this->assertSame($data['email'], $response->data->email);

        return $response->data;
    }

	/**
	 * @depends testCreateProject
	 * @param $project
	 * @var \EngagementSdk\Response $response
	 */
	public function testListMembers($project) {
		$response = $this->client
			->member()
			->where(['project.id','=',$project->hash_id])
			->get(true);

		if ($response->isError()) {
			$response->errorToException();
		}

		$this->assertTrue(is_array($response->data));
	}

	/**
	 * @depends testCreateProject
	 * @depends testCreateAction
	 * @param $project
	 * @param $action
	 * @return
	 */
	public function testFilter($project, $action) {
		$response = $this->client
			->action()
			->filter(['name','=','test_action'])
			->filter(['project.hash_id', '=', $project->hash_id])
			->get(true);

		$this->assertCount(1, $response->data);

		$this->assertEquals('test_action', $response->data[0]->name);
		$this->assertEquals($project->hash_id, $response->data[0]->project->data->hash_id);
		$this->assertEquals($action->hash_id, $response->data[0]->hash_id);

		return $response->data;

	}

	/**
	 * @depends testCreateAction
	 * @depends testIdentifyMemberCreate
	 * @param $member
	 * @param $action
	 */
	public function testSingleEvent($action, $member) {
		$data = [
			'eventMetaData' => [[
				'name' => 'transaction',
				'value' => (object)[
					'amount' => 1873,
					'$unique_id' => 12791723,
					'txn_date' => 12476765431,
					'due_data' => 12476765431
				]
			]]
		];

		$response = $this->client
			->action($action->hash_id)
			->member($member->hash_id)
			->event()
			->create($data);

		$this->assertSame( 0, $response->data->status);
	}

    /**
     * @depends testCreateProject
     * @param $project
     */
    public function testFireEventBatch($project) {
        $one = new Event('test_action');
        $one->setCustomerEmail('customer_1@example.org')
            ->setCustomerId(time())
            ->addData('transaction', [
                'amount' => 123,
                '$unique_id' => '1234',
                'txn_date' => 1473741280,
                'due_date' => 1473741280
            ]);

        $two = new Event('test_action');
        $two->setCustomerEmail('customer_2@example.org')
            ->setCustomerId(time() + 1)
            ->addData('transaction', [
                'amount' => 999,
                '$unique_id' => '4321',
                'txn_date' => 1473741281,
                'due_date' => 1473741281
            ]);

        $data = [$one, $two];

	    $response = $this->client
		    ->project($project->hash_id)
		    ->event()
		    ->fireBatch($data, true, true);

	    if ($response->isError()) {
		    $response->errorToException();
	    }

	    $this->assertObjectNotHasAttribute('error', $response->meta);
    }



    public function testListCalculatorFunctions() {
        $response = $this->client
            ->rule()
            ->calculatorFunction()
            ->get();

        if ($response->isError()) {
            $response->errorToException();
        }

        $this->assertTrue(is_array($response->data));
        $this->assertGreaterThan(1, count($response->data));
    }

    public function testListConditionExpressions() {
        $response = $this->client
            ->rule()
            ->conditionExpression()
            ->get();

        if ($response->isError()) {
            $response->errorToException();
        }

        $this->assertTrue(is_array($response->data));
	    $this->assertGreaterThan(1, count($response->data));
    }


    /**
     * @depends testCreateProject
     * @param $project
     */
    public function testCreateReward($project) {
        $name = 'Coins';

        $response = $this->client
            ->project($project->hash_id)
            ->reward()
            ->create([
                'name' => $name,
            ]);

        if ($response->isError()) {
            $response->errorToException();
        }

        $this->assertSame($response->data->name, $name);

        return $response->data;
    }


    /**
     * @depends testCreateProject
     * @depends testCreateReward
     * @param $project
     * @param $reward
     */
    public function testCreateStatusLevel($project, $reward) {
        $name = 'Silver';
        $status_reward = [
            'name' => $name,
            'status_level' => true,
            'status_level_base' => $reward->name,
            'status_level_base_min_quantity' => 50,
        ];

        $response = $this->client
            ->project($project->hash_id)
            ->reward()
            ->create($status_reward)
            ->errorToException();
        $this->assertSame($response->data->name, $name);

        $status_reward['status_level_base_min_quantity'] = 100;
        $status_reward['name'] = 'Gold';
        $response = $this->client
            ->project($project->hash_id)
            ->reward()
            ->create($status_reward)
            ->errorToException();
        $this->assertObjectNotHasAttribute('error', $response->meta);

        return $response->data;
    }


    /**
     * @depends testCreateProject
     * @param $project
     */
    public function testCreateQuestion($project) {
        $title = 'Test Question';
        $question = 'Is this question an adequate test for this method?';

        $response = $this->client->addQuestion($project->hash_id, $title, $question)->errorToException();
        $this->assertObjectNotHasAttribute('error', $response->meta);

        return $response->data;
    }


    /**
     * @depends testCreateQuestion
     * @param $reward
     */
    public function testGetQuestion($reward) {
        $response = $this->client->getQuestion($reward->hash_id)->errorToException();
        $this->assertObjectNotHasAttribute('error', $response->meta);

        return $response->data;
    }


    /**
     * @depends testCreateQuestion
     * @param $reward
     */
    public function testDeactivateQuestion($reward) {
        $response = $this->client->deactivateQuestion($reward->hash_id)->errorToException();
        $this->assertObjectNotHasAttribute('error', $response->meta);

        return $response->data;
    }


    /**
     * @depends testCreateProject
     * @param $project
     */
    public function testGetQuestions($project) {
        // make some questions
        for ($i = 0; $i < 5; $i++) {
            $title = "Question $i";
            $question = "This is question number $i in a series of questions";
            $reward = $this->client->addQuestion($project->hash_id, $title, $question)->errorToException();
            $this->assertObjectNotHasAttribute('error', $reward->meta);
        }
        $response = $this->client->getQuestions($project->hash_id)->errorToException();
        $this->assertObjectNotHasAttribute('error', $response->meta);

        return $response->data;
    }


    /**
     * @depends testIdentifyMemberCreate
     * @param $member
     */
    public function testGetMemberRatings($member) {
        $response = $this->client->getMemberRatings($member->hash_id)->errorToException();
        $this->assertObjectNotHasAttribute('error', $response->meta);

        return $response->data;
    }


    /**
     * @depends testIdentifyMemberCreate
     * @param $member
     * @depends testCreateProject
     * @param $project
     */
    public function testUpdateMemberRating($member, $project) {
        $title = 'Exam Question';
        $question = 'Does this question count as an adequate test for this method?';
        $reward = $this->client->addQuestion($project->hash_id, $title, $question)->errorToException();
        $this->assertObjectNotHasAttribute('error', $reward->meta);
        $data = base64_encode(json_encode([
            'rating' => 5,
            'comment' => 'this is a comment'
        ]));
        $response = $this->client->updateMemberRating($member->hash_id, $reward->data->hash_id, $data)->errorToException();
        $this->assertObjectNotHasAttribute('error', $response->meta);

        return $response->data;
    }


    /**
     * @depends testUpdateMemberRating
     * @param $event
     */
    public function testGetMemberRatingsHistory($event) {
        // deactivating the question shouldn't affect the history
        $this->client->deactivateQuestion($event->action->data->hash_id)->errorToException();

        $response = $this->client->getMemberRatingsHistory($event->member->data->hash_id, $event->action->data->hash_id)->errorToException();
        $this->assertObjectNotHasAttribute('error', $response->meta);

        return $response->data;
    }

    /**
     * @depends testIdentifyMemberCreate
     * @param $member
     * @depends testCreateProject
     * @param $project
     */
    public function testFireMemberEvent($member, $project) {
        $action_name = 'some_action';
        $response = $this->client->fireMemberEvent($action_name, $member->hash_id, $project->hash_id);
        $this->assertObjectNotHasAttribute('error', $response->meta);
//        print_r($response->data);

        return $response->data;
    }

    /**
     * @depends testIdentifyMemberCreate
     * @param $member
     * @depends testCreateProject
     * @param $project
     */
    public function testFireMemberEventWithData($member, $project) {
        $action_name = 'some_action';
        $meta = [
            [
                'name' => 'rating',
                'value' => 11
            ],
            [
                'name' => 'description',
                'value' => 'this is a rating out of 10'
            ]
        ];
        $response = $this->client->fireMemberEvent($action_name, $member->hash_id, $project->hash_id, $meta);
        $this->assertObjectNotHasAttribute('error', $response->meta);
//        print_r($response->data);

        return $response->data;
    }



    /**
     * @depends testCreateProject
     * @param $project
     */
    public function testDeleteProject($project) {
        $response = $this->client
            ->project($project->hash_id)
            ->delete();

        if ($response->isError()) {
            $response->errorToException();
        }

        $this->assertInstanceOf(\EngagementSdk\Response::class, $response);
    }

}

