<?php

namespace EngagementSdk;


/*******************************************************************************
 * object of this class that is returned for each API request
 * and it contains all data from api request in natural structure
 * plus - list of functions, like "errorToException"
 ******************************************************************************/
class Response
{

	public $data;
	public $meta;

	public function __construct(\stdClass $childObject = null)
	{
		if (isset($childObject)) {
			$this->data = $childObject->data;
			$this->meta = $childObject->meta;
		}
	}

	public function isError()
	{
		return isset($this->meta->error);
	}

	public function errorToException()
	{
		if ($this->isError()) {
			throw new Exception($this->meta->error->reason);
		}

		return $this;
	}
}
