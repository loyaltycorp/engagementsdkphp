<?php

namespace EngagementSdk;

class Exception extends \Exception
{
	protected $request;

	public function __construct($message, $code = 0, Exception $previous = null, $request = null)
	{
		parent::__construct($message, $code, $previous);
		$this->setRequest($request);
	}

	public function setRequest($request)
	{
		$this->request = $request;
	}

	public function getRequest()
	{
		return $this->request;
	}

}
