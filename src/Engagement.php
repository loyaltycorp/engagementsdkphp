<?php

namespace EngagementSdk;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Loyalty Corp Engagement Platform API
 *
 * https://engagement-api-staging.loyaltycorp.com.au/api/documentation
 *
 */
class Engagement
{

	use Rentalia;

	/**
	 * Engagement client
	 * @var Client Object
	 */
	protected $client;

	/**
	 * Engagement Operator (vendor, projects, rules, etc...)
	 * @var String
	 */
	protected $operator;

	/**
	 * The name of the vendor to masquerade as. Used with an Admin API key to operate on vendor projects.
	 * @var String
	 */
	protected $masquerade;

	/**
	 * An authorisation array (username, password) for certain requests.
	 * @var array
	 */
	protected $authArr = [];

	/**
	 * Engagement query parameters
	 * @var array
	 */
	protected $query = [];

	/**
	 * @var array
	 */
	protected $filter = [];

	/**
	 * @var int
	 */
	protected $pageSize = 500;

	/**
	 * Engagement available operators
	 * @var array
	 */
	protected $operators = [
		'rule' => 'rules',
		'award' => 'awards',
		'reward' => 'rewards',
		'vendor' => 'vendors',
		'action' => 'actions',
		'member' => 'members',
		'project' => 'projects',
		'webhook' => 'webhooks',
		'particular' => 'particulars',
		'redemption' => 'redemptions',
		'redemptionProgram' => 'redemptionPrograms',
		'conditionOperation' => 'conditionOperations',
		'event' => 'events',
		'expiration' => 'expirations',
		'statusLevels' => 'statusLevels',
		'calculatorFunction' => 'calculatorFunctions',
		'conditionExpression' => 'conditionExpressions',
		'template' => 'templates',
		'badge' => 'badges',
	];

	/**
	 * Initialise class
	 *
	 * @param string $endpoint
	 * @param string $apiKey
	 * @param array $authArr
	 */
	public function __construct(string $endpoint, string $apiKey, array $authArr = [])
	{
		if ( ! empty($authArr)) {
			$this->setAuthArr($authArr);
		}
		$this->client = new Client([
			'base_uri' => $endpoint,
			'query' => ['api_key' => $apiKey],
			'headers' => ['Accept' => 'application/json'],
		]);
	}


	/**
	 * Dynamic method declaration based on preset Operators
	 *
	 * @param string $operator_name
	 * @param array $parameters
	 *
	 * @return Engagement response
	 */
	public function __call($operator_name, array $parameters = [])
	{
		if ($this->getOperator($operator_name)) {
			$operator_name .= 's';
		}
		$id = ! empty($parameters) ? $parameters[0] : null;
		$this->setOperator($operator_name, $id);

		return $this;
	}

	/**
	 * Adds Basic Auth to the request, with array containing both username and password
	 *
	 * @param array $authArr
	 *
	 * @return Engagement
	 */
	public function setAuthArr(array $authArr): Engagement
	{
		$this->authArr = $authArr;

		return $this;
	}

	/**
	 * @param $number
	 */
	public function setPageSize($number)
	{
		$this->pageSize = $number;
	}

	public function getPageSize(): int
	{
		return $this->pageSize;
	}


	/**
	 * @param $param
	 * @param $number
	 *
	 * @return Engagement $this
	 */
	public function setParam($param, $number): Engagement
	{
		$this->query = array_merge(
			$this->query,
			[$param => $number]
		);

		return $this;
	}


	/***********************-- Main Request Operations --**********************/

	/**
	 * Global request HTTP request
	 *
	 * @param string $method HTTP method request
	 * @param string $uri route/url endpoint
	 * @param array $data
	 * @param boolean $clearState When true clear the filter, query, masquerade and operator after processing the request
	 *
	 * @return Response Obj
	 */
	private function request(string $method, string $uri, array $data = [], $clearState = false): Response
	{
		$this->operator = null;
		// Form parameters for POST and PUT
		if ($data) {
			if ( ! isset($data['multipart'])) {
				$data = array_merge($data, ['form_params' => $data]);
			}
		}
		// Query parameters
		if ($this->query || $this->masquerade) {
			$query = $this->client->getConfig('query');
			if ($this->query) {
				$query       = array_merge(
					$query,
					$this->query
				);
				$this->query = [];
			}
			if ($this->masquerade) {
				$query            = array_merge($query, ['as_user' => $this->masquerade]);
				$this->masquerade = null;
			}
			if ($this->authArr) {
				$data = array_merge($data, ['auth' => $this->authArr]);
			}
			if (count($this->filter) > 0) {
				$query = array_merge($query, $this->formatFilter());
			}
			$data = array_merge($data, ['query' => $query]);
		}

		$request         = new \stdClass;
		$request->uri    = $uri;
		$request->data   = $data;
		$request->method = $method;

		try {
			$response = $this->client->request($method, $uri, $data);
			$result   = json_decode($response->getBody());
			if (strtolower($method) === 'delete') {
				return new Response();
			}

			if ( ! isset($result->meta)) {
				$result->meta = $this->buildMeta();
//                $message = 'Meta-tag was not found in response, when requesting: ('.json_encode($request).')';
			}
			$result->meta->requested = $request;
		} catch (RequestException $e) {
			$result = $this->handleError($e, $request);
		}

		$result = new Response($result);
		if ($clearState) {
			$this->clearState();
		}

		return $result;
	}


	/**
	 * Global handle method for exception behaviour
	 *
	 * @param RequestException $e
	 * @param $request
	 *
	 * @return \stdClass
	 */
	private function handleError($e, $request): \stdClass
	{
		$errorCode   = 0;
		$errorReason = 'Engagement Platform is unavailable';
		if ($e->hasResponse()) {
			$errorObject = json_decode($e->getResponse()->getBody());
			if ($errorObject) {
				$errorCode   = $errorObject->data->code;
				$errorReason = $errorObject->data->message;
			} else {
				$errorCode   = $e->getResponse()->getStatusCode();
				$errorReason = $e->getResponse()->getReasonPhrase();
			}
//            $message = 'Engagement Platform error: '.$errorReason.', Code: '.$errorCode;
		}
		$response                      = new \stdClass;
		$response->data                = [];
		$response->meta                = $this->buildMeta();
		$response->meta->error         = new \stdClass;
		$response->meta->error->code   = $errorCode;
		$response->meta->error->reason = $errorReason;
		$response->meta->requested     = $request;

		return $response;
	}

	/**
	 * method for building metadata for response
	 *
	 * @return \stdClass
	 */
	private function buildMeta(): \stdClass
	{
		$meta                           = new \stdClass;
		$meta->pagination               = new \stdClass;
		$meta->pagination->total        = 0;
		$meta->pagination->per_page     = 10;
		$meta->pagination->current_page = 1;

		return $meta;
	}

	/**
	 * Set operator (URI builder)
	 *
	 * @param string $operator
	 * @param string $id (nullable)
	 *
	 * @return Engagement
	 */
	private function setOperator(string $operator, string $id = null): Engagement
	{
		if ($this->operator) {
			// Existing Operators have precedence over current Operator
			$this->operator .= '/' . $operator;
		} else {
			$this->operator = $operator;
		}
		if ($id) {
			// Add ID to operator
			$this->operator .= '/' . $id;
		}

		return $this;
	}

	/***************************-- Custom Operations --*************************/

	/**
	 * @return array
	 */
	protected function formatFilter(): array
	{
		$data = [];
		$i    = 0;
		foreach ($this->filter as $filter) {
			$data["filter[{$i}]"] = $filter[0] . ',' . $filter[1] . ',' . $filter[2];
			$i++;
		}

		return $data;
	}

	/**
	 * Filtering query operator
	 *
	 * @param array $data
	 *
	 * @return Engagement response
	 */
	public function filter(array $data): Engagement
	{
		$this->filter[] = $data;

		return $this;
	}


	/**
	 * Filtering Alias
	 *
	 * @param array $data
	 *
	 * @return Engagement response
	 */
	public function where(array $data): Engagement
	{
		$this->filter($data);

		return $this;
	}


	/**
	 * Pagination query operator
	 *
	 * @param int $page_number
	 *
	 * @return Engagement response
	 */
	public function page($page_number): Engagement
	{
		return $this->setParam('page', $page_number);
	}


	/**
	 * Pagination query operator
	 *
	 * @param int $page_size
	 *
	 * @return Engagement response
	 */
	private function pageSize($page_size): Engagement
	{
		$this->setPageSize($page_size);

		return $this->setParam('pageSize', $page_size);
	}


	/**
	 * Pagination query operator
	 *
	 * @param int $pageSize (nullable)
	 *
	 * @return Response
	 */
	public function paginate($pageSize = null): Response
	{
		if ($pageSize) {
			$this->pageSize($pageSize);
		}
		$currentPage = 1;
		$query       = $_SERVER['QUERY_STRING'];
		if ( ! empty($query)) {
			foreach (explode('&', $query) as $param) {
				$pair = explode('=', $param);
				if ($pair[0] === 'page') {
					$currentPage = $pair[1];
					break;
				}
			}
		}

		return $this->page($currentPage)->getInternal();
	}


	/**
	 * Order / sorting query operator
	 *
	 * @param string $field
	 * @param string $order defaults to ascending
	 *
	 * @return Engagement response
	 */
	public function sortBy(string $field, string $order = 'asc'): Engagement
	{
		$order       = ($order !== 'desc') ? 'asc' : 'desc';
		$this->query = array_merge(
			$this->query,
			['order' => [$field . ',' . $order]]
		);

		return $this;
	}

	/**
	 * Order / sorting query operator
	 * (because why???)
	 *
	 * @param string $field
	 *
	 * @return Engagement response
	 */
	public function sortByDesc(string $field): Engagement
	{
		return $this->sortBy($field, 'desc');
	}

	/*
	 * Include query operator
	 * comma separated values - eg: project,action
	 *
	 * @param string $fields
	 * @return $this
	 */
	public function including(string $fields)
	{
		$this->query = array_merge(
			$this->query,
			['include[]' => $fields]
		);

		return $this;
	}


	/**
	 * Set masquerade Vendor (to Act in behalf of another vendor)
	 *
	 * @param string $vendor ID / name
	 *
	 * @return Engagement response
	 */
	public function maskAs(string $vendor): Engagement
	{
		$this->masquerade = $vendor;

		return $this;
	}

	/**
	 * Get masquerade Vendor
	 *
	 * @return string response
	 */
	public function getMask(): string
	{
		return $this->masquerade;
	}

	/**
	 * @param string $name
	 *
	 * @return mixed|null
	 */
	private function getOperator($name)
	{
		return $this->operators[$name] ?? null;
	}

	/**
	 *
	 */
	private function clearState()
	{
		$this->operator   = '';
		$this->masquerade = false;
		$this->filter     = [];
		$this->query      = [];
	}

	/**
	 * Get vendor details
	 *
	 * @param boolean $clearState
	 *
	 * @return Response response
	 */
	private function getInternal($clearState = false): Response
	{
		$operator = $this->operator;

		return $this->request('GET', $operator, [], $clearState);
	}

	/** Special Case - used with identify
	 *
	 * @param array $data
	 *
	 * @return Response response
	 */
	public function identify(array $data = []): Response
	{
		$operator = $this->operator . '/identify/' . base64_encode(json_encode($data));

		return $this->pageSize($this->pageSize)
		            ->request('GET', $operator);
	}

	/**
	 * @param bool $clearState
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function get($clearState = false): Response
	{
		$result = $this->pageSize($this->pageSize)->getInternal($clearState);
		//check if it is a collection
		if ( ! is_array($result->data)) {
			throw new Exception('Use "find" to receive a single object');
		}

//        self::checkIfReturnedLessThanExpected($result);
		return $result;
	}

	/**
	 * it takes only 500 records ?
	 * if there are more - it will throw exception/Bugsnag
	 * if you need more as a quick fix you can change pageSize
	 * however, you probably need to consider pagination (use paginate method)
	 * It is good practice to limit the number of records returned in one request,
	 * always use pagination and use reasonable page sizes. You would need
	 * good motivation to request pages larger than 100 records.
	 *
	 * @param $result
	 *
	 * @throws Exception
	 */
	private static function checkIfReturnedLessThanExpected($result)
	{
		// check if it returned less than expected
		if ($result->meta->pagination->total > $result->meta->pagination->per_page) {
			$message =
				'Received ' . $result->meta->pagination->per_page
				. ' items, when there should be ' . $result->meta->pagination->total
				. '. For class="' . $result->meta->class . '", '
				. ' type="' . $result->meta->resource_type . '".';
			throw new Exception($message);
		}
	}


	/**
	 * Request single object - without clearing the filter, query, masquerade and operator after processing the request
	 * @return Response
	 * @throws Exception
	 */
	public function find(): Response
	{
		return $this->findInternal(false);
	}


	/**
	 * Request single object - and clear the filter, query, masquerade and operator after processing the request
	 * @return Response
	 * @throws Exception
	 */
	public function findOrFail(): Response
	{
		return $this->findInternal(true);
	}


	/**
	 * Request single object
	 *
	 * @param $clearState
	 *
	 * @return Response
	 * @throws Exception
	 */
	private function findInternal($clearState): Response
	{
		$result = $this->getInternal($clearState);
		if (isset($result->meta->error)) {
			$result->data = null;
		}
		if (is_array($result->data)) {
			throw new Exception('Use "get" to receive an array of objects.');
		}

		return $result;
	}

	/**
	 * Create
	 *
	 * @param array $data Values
	 *
	 * @return Response $response
	 */
	public function create(array $data): Response
	{
		return $this->request('POST', $this->operator, $data);
	}


	/**
	 * Update
	 *
	 * @param array $data Values
	 *
	 * @return Response response
	 */
	public function update(array $data): Response
	{
		return $this->request('PUT', $this->operator, $data);
	}


	/**
	 * Delete
	 *
	 * @return Response response
	 */
	public function delete(): Response
	{
		return $this->request('DELETE', $this->operator);
	}


	/*
	 * linking functions: Rule -> Reward
	 * PUT /rules/{ruleId}/linkReward/{rewardId}
	 */
	public function linkRewardToRule($ruleId, $rewardId, $data): Response
	{
		$this->setOperator('rules', $ruleId)
		     ->setOperator('linkReward', $rewardId);

		return $this->request('PUT', $this->operator, $data);
	}


	/**
	 * unlinking function: Rule -> Reward
	 * DELETE /rules/{ruleId}/unlinkReward/{rewardId}
	 *
	 * @param $ruleId
	 * @param $rewardId
	 *
	 * @return Response
	 */
	public function unlinkRewardFromRule($ruleId, $rewardId): Response
	{
		$this->setOperator('rules', $ruleId)
		     ->setOperator('unlinkReward', $rewardId);

		return $this->request('DELETE', $this->operator);
	}


	/**
	 * linking function: Rule -> Action
	 * PUT /actions/{actionId}/linkRule/{ruleId}
	 *
	 * @param $actionId
	 * @param $ruleId
	 *
	 * @return Response
	 */
	public function linkRuleToAction($actionId, $ruleId): Response
	{
		$this->setOperator('actions', $actionId)
		     ->setOperator('linkRule', $ruleId);

		return $this->request('PUT', $this->operator);
	}


	/**
	 * unlinking function: Rule -> Action
	 * DELETE /actions/{actionId}/unlinkRule/{ruleId}
	 *
	 * @param $actionId
	 * @param $ruleId
	 *
	 * @return Response
	 */
	public function unlinkRuleFromAction($actionId, $ruleId): Response
	{
		$this->setOperator('actions', $actionId)
		     ->setOperator('unlinkRule', $ruleId);

		return $this->request('DELETE', $this->operator);
	}


	/**
	 * linking function: Redemption program -> Reward
	 * PUT /redemptionPrograms/{programId}/linkReward/{rewardId}
	 *
	 * @param $redemptionProgramId
	 * @param $rewardId
	 * @param array $data
	 *
	 * @return Response
	 */
	public function linkRewardToRedemptionProgram($redemptionProgramId, $rewardId, array $data): Response
	{
		$this->setOperator('redemptionPrograms', $redemptionProgramId)
		     ->setOperator('linkReward', $rewardId);

		return $this->request('PUT', $this->operator, $data);
	}

	/**
	 * unlinking function: Redemption program -> Action
	 * DELETE /redemptionPrograms/{programId}/unlinkReward/{rewardId}
	 *
	 * @param $redemptionProgramId
	 * @param $rewardId
	 *
	 * @return Response
	 */
	public function unlinkRewardFromRedemptionProgram($redemptionProgramId, $rewardId): Response
	{
		$this->setOperator('redemptionPrograms', $redemptionProgramId)
		     ->setOperator('unlinkReward', $rewardId);

		return $this->request('DELETE', $this->operator);
	}

	/**
	 * Award points to a reward for a member
	 * POST /members/{memberId}/rewards/{rewardId}/award
	 *
	 * @param string $memberId Id of member
	 * @param string $rewardId Id of reward
	 * @param array $data Additional data
	 *
	 * @return Response response
	 */
	public function awardPointsToRewardForMember($memberId, $rewardId, $data): Response
	{
		$this->setOperator('members', $memberId)
		     ->setOperator('rewards', $rewardId)
		     ->setOperator('award');

		foreach ($data as $k => $v) {
			$this->setParam($k, $v);
		}

		return $this->request('POST', $this->operator);
	}

	/**
	 * INITIALISE A NEW SDK CLIENT with token retrieved from the project hash ID
	 *
	 * @param string $endPoint
	 * @param string $projectId
	 *
	 * @return Engagement
	 * @throws Exception
	 */
	public function getProjectScopedClient($endPoint, $projectId): Engagement
	{
		$token = $this->getProjectApiKey($projectId);

		return new self($endPoint, $token);
	}

	/**
	 * Returns the project token for a given project hash ID
	 *
	 * @param string $projectId
	 *
	 * @return string
	 * @throws Exception
	 */
	protected function getProjectApiKey($projectId): string
	{
		$this->setOperator('projects', $projectId);
		$result = $this->request('GET', $this->operator);
		if ($result === null || empty($result) || isset($result->meta->error)) {
			throw new Exception("could not find a project with id $projectId", 404);
		}

		return $result->data->token;
	}

	/**
	 * @param array $events
	 * @param bool $createMembers
	 * @param bool $runSync
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function fireBatch($events, $createMembers = true, $runSync = false): Response
	{
		$data = [];
		foreach ($events as $event) {
			if (get_class($event) !== Event::class && ! is_subclass_of($event, Event::class)) {
				throw new Exception('$events should be an array of event objects.', 400);
			}
			$data[] = $event->asArray();
		}

		$tempFile = tempnam(sys_get_temp_dir(), 'foo');
		$handle = fopen($tempFile, 'wb');
		fwrite($handle, json_encode($data));
		fclose($handle);

		$this->setParam('createMembers', $createMembers);
		$this->setParam('run_sync', $runSync);

		$data     = ['multipart' => [['name' => 'batch', 'contents' => fopen($tempFile, 'rb')]]];
		$response = $this->request('POST', $this->operator . '/fireBatch', $data);
		unlink($tempFile);

		return $response;
	}

	/**
	 * @param $action_name
	 * @param $member_hash_id
	 * @param $project_hash_id
	 * @param array $eventData (optional)
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function fireMemberEvent($action_name, $member_hash_id, $project_hash_id, array $eventData = []): Response
	{
		$actions        = $this
			->action()
			->where(['name', '=', $action_name])
			->where(['project.hash_id', '=', $project_hash_id])
			->get(true)
			->errorToException();
		$action_hash_id = false;
		if ( ! empty($actions->data)) {
			foreach ($actions->data as $action) {
				if ($action->name === $action_name) {
					$action_hash_id = $action->hash_id;
					break;
				}
			}
		}
		if ( ! $action_hash_id) {
			$action = (object)['name' => $action_name];
			$this->setOperator('projects', $project_hash_id)
			     ->setOperator('actions');
			$new_action = $this->request('POST', $this->operator, (array)$action);
			if (isset($new_action->meta->error)) {
				throw new Exception($new_action->meta->error->reason);
			}
			$this->operator = '';
			$action_hash_id = $new_action->data->hash_id;
		}
		$this->setOperator('actions', $action_hash_id)
		     ->setOperator('members', $member_hash_id)
		     ->setOperator('events');
		$result = $this->request('POST', $this->operator, ['eventMetaData' => $eventData]);
		if (isset($result->meta->error)) {
			throw new Exception($result->meta->error->reason);
		}

		return $result;
	}
}

