<?php

namespace EngagementSdk;

trait Rentalia
{


	/**
	 * Create a new 'Question' type Reward (PRR thing)
	 *
	 * @param string $project_hash_id
	 * @param string $title
	 * @param string $text
	 * @param int $minQ
	 * @param int $maxQ
	 * @param string $group
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function addQuestion($project_hash_id, $title, $text, $minQ = 0, $maxQ = 10, $group = 'Q'): Response
	{
		$name          = '_' . $group . '_' . $title;
		$reward        = [
			'name' => $name,
			'description' => $text,
			'min_quantity' => $minQ,
			'max_quantity' => $maxQ,
		];
		$result_reward = $this->project($project_hash_id)->reward()->create($reward);
		if ($result_reward === null || isset($result_reward->meta->error)) {
			$error_msg = isset($result_reward->meta->error) ? $result_reward->meta->error->reason : 'could not create question';
			throw new Exception($error_msg);
		}
		$rule   = [
			'name' => $name,
			'actions' => [$name],
			'rewards' => ["$name=-1 * MemberAwardCount($name) + 1 * EventMetadata(rating)"],
		];
		$result = $this->project($project_hash_id)->rule()->create($rule);
		if ($result === null || isset($result->meta->error)) {
			$error_msg = isset($result->meta->error) ? $result->meta->error->reason : 'could not create question';
			throw new Exception($error_msg);
		}

		return $result;
	}

	/**
	 * Get a 'Question' type Reward (PRR thing)
	 *
	 * @param $hash_id
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function getQuestion($hash_id)
	{
		$result = $this->reward($hash_id)->findOrFail();
		if ($result === null || isset($result->meta->error)) {
			$error_msg = isset($result->meta->error) ? $result->meta->error->reason : 'could not find question';
			throw new Exception($error_msg);
		}

		return $result;
	}

	/**
	 * Get a list of 'Question' type Rewards (PRR thing)
	 *
	 * @param string $project_hash_id
	 * @param string $group
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function getQuestions($project_hash_id, $group = 'Q'): Response
	{
		$actions = $this
			->action()
			->where(['project.hash_id', '=', $project_hash_id])
			->where(['name', 'like', '_' . $group . '_%'])
			->where(['active', '=', 1])
			->get(true);
		if ($actions === null || isset($actions->meta->error)) {
			$error_msg = isset($actions->meta->error) ? $actions->meta->error->reason : 'Guru Mediation';
			throw new Exception($error_msg);
		}
		$result = $this
			->reward();
		if ( ! empty($actions->data)) {
			$hashes = [];
			foreach ($actions->data as $action) {
				$hashes[] = $action->hash_id;
			}
			$result = $result->where(['hash_id', '=', implode(',', $hashes)]);
		} else {
			// force an empty set
			$result = $result->where(['id', '=', 0]);
		}
		$result = $result->get();

		return $result;
	}

	/**
	 * Set 'Question' type Action (PRR thing) to inactive (essentially delete)
	 *
	 * @param string $hash_id (Reward hash ID)
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function deactivateQuestion($hash_id): Response
	{
		// convert Reward into Action
		$result = $this->action($hash_id)->findOrFail();
		if ($result === null || isset($result->meta->error)) {
			$error_msg = isset($result->meta->error) ? $result->meta->error->reason : 'could not find question';
			throw new Exception($error_msg);
		}
		$action           = (array)$result->data;
		$action['active'] = false;
		$update           = $this->action($hash_id)->update($action);
		if ($update === null || isset($update->meta->error)) {
			$error_msg = isset($update->meta->error) ? $update->meta->error->reason : 'could not update question';
			throw new Exception($error_msg);
		}

		return $update;
	}

	/**
	 * Get all awards for reward group, by member ID
	 *
	 * @param $member_id
	 * @param string $group
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function getMemberRatings($member_id, $group = 'Q'): Response
	{
		$result = $this
			->member($member_id)
			->award()
			->where(['reward.name', 'like', '_' . $group . '_%'])
			->get(true);
		if ($result === null || isset($result->meta->error)) {
			$error_msg = isset($result->meta->error) ? $result->meta->error->reason : 'could not find member';
			throw new Exception($error_msg);
		}

		return $result;
	}

	/**
	 * @param string $member_id
	 * @param string $question_id (Reward hash ID - should be identical to Action hash ID)
	 * @param $data (base-64 encoded json)
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function updateMemberRating($member_id, $question_id, $data): Response
	{
		$result = $this->action($question_id)->findOrFail();
		if ($result === null || isset($result->meta->error)) {
			$error_msg = isset($result->meta->error) ? $result->meta->error->reason : 'could not find question';
			throw new Exception($error_msg);
		}
		if ( ! $result->data->active) {
			throw new Exception('question has been deactivated');
		}
		if ($data_items = json_decode(base64_decode($data))) {
			if (isset($data_items->comment)) {
				$rating = isset($data_items->rating) ? $data_items->rating : 0;
				$meta   = [
					[
						'name' => 'rating',
						'value' => $rating
					],
					[
						'name' => 'description',
						'value' => $data_items->comment
					]
				];
				$this->setOperator('actions', $question_id)
				     ->setOperator('members', $member_id)
				     ->setOperator('events');
				$result = $this->request('POST', $this->operator, ['eventMetaData' => $meta]);
			}
		}
		if ($result === null || isset($result->meta->error)) {
			$error_msg = isset($result->meta->error) ? $result->meta->error->reason : 'invalid data';
			throw new Exception($error_msg);
		}

		return $result;
	}

	/**
	 * @param $member_id
	 * @param string $question_id (Reward hash ID)
	 * @param string $startDate (optional)
	 * @param string $endDate (optional)
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function getMemberRatingsHistory($member_id, $question_id, $startDate = '', $endDate = ''): Response
	{
		$result = $this
			->member($member_id)
			->award()
			->where(['reward.hash_id', '=', $question_id]);
		if ( ! empty($startDate)) {
			$result->where(['created_at', '>=', $startDate]);
		}
		if ( ! empty($endDate)) {
			$result->where(['created_at', '<=', $endDate]);
		}
		$result = $result->get(true);
		if ($result === null || isset($result->meta->error)) {
			$error_msg = isset($result->meta->error) ? $result->meta->error->reason : 'could not find member';
			throw new Exception($error_msg);
		}

		return $result;
	}
}