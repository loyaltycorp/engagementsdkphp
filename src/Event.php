<?php

namespace EngagementSdk;


/**
 * wrapper class for an event
 */
class Event
{

	protected $name;
	protected $memberHashId;
	protected $date;
	protected $customer = [];
	protected $data = [];

	public function __construct($action)
	{
		$this->name = $action;
	}

	public function setMemberHashId($id)
	{
		$this->memberHashId = $id;

		return $this;
	}

	public function setCustomerId($id)
	{
		$this->customer['id'] = $id;

		return $this;
	}

	public function getCustomerId()
	{
		return $this->customer['id'];
	}

	public function setCustomerEmail($email)
	{
		$this->customer['email'] = $email;

		return $this;
	}

	public function setDate($iso8601)
	{
		$this->date = $iso8601;

		return $this;
	}

	public function addData($name, $value)
	{
		$this->data[] = ['name' => $name, 'value' => $value];

		return $this;
	}

	public function asArray(): array
	{
		return [
			'name' => $this->name,
			'customer' => $this->customer,
			'date' => $this->date,
			'data' => $this->data
		];
	}


	public function getName()
	{
		return $this->name;
	}

	public function getData()
	{
		return $this->data;
	}
}